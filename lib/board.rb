class Board
  def initialize(grid = [])
    @grid = grid

    if @grid == []
      @grid = Array.new(3) { |array| Array.new(3)}
    end
  end

  def grid
    @grid
  end

  def place_mark(position, mark)
    x = position[0]
    y = position[1]
    @grid[x][y] = mark
  end

  def empty?(position)
    x = position[0]
    y = position[1]
    return true if @grid[x][y] == nil
    false
  end

  def winner
    #row win
    @grid.each do |row|
      if row.all? { |position| position == row[0] && !row[0].nil? }
        return row[0]
      end
    end

    #left diagonal win
    left_diagnol = []
    position = 0
    while position < @grid.length
      left_diagnol << @grid[position][position]
      position += 1
    end

    if left_diagnol.all? { |position| position == left_diagnol[0] && !left_diagnol[0].nil?}
      return left_diagnol[0]
    end

    #right diagonal win
    right_diagnol = []
    position = 0
    while position < @grid.length
      right_diagnol << @grid[position][@grid.length - position - 1]
      position += 1
    end
    if right_diagnol.all? { |position| position == right_diagnol[0] && !right_diagnol[0].nil?}
      return right_diagnol[0]
    end

    #column win
    @grid.transpose.each do |column|
      if column.all? { |position| position == column[0] && !column[0].nil?}
        return column[0]
      end
    end
    nil
  end

  def over?
    return true if !winner.nil?
    return true if !@grid.any? { |array| array.include?(nil) }
    false
  end

  def size
    count = 0
    @grid.each {|array| count += 1}
    count
  end
end
