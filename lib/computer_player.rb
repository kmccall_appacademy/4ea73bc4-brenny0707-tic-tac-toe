class ComputerPlayer
  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def board
    @board
  end

  def mark=(mark)
    @mark = mark
  end

  def get_move

    empty_spots = []
    @board.grid.each_with_index do |row, idx|
      row.each_with_index do |col, cidx|
        theory_grid = @board.grid.dup
        position = @board.grid[idx][cidx]
        empty_spots << [idx, cidx] if position.nil?
      end #row.each
    end #@board.grid.each

    empty_spots.each do |position|
      theory_grid = @board.grid.dup
      temp_element = theory_grid[position[0]][position[1]]
      theory_grid[position[0]][position[1]] = @mark
      if @board.winner == @mark
        return position
      else
        theory_grid[position[0]][position[1]] = temp_element
      end #if
    end #empty_spots do
    empty_spots.shuffle.first
  end #get_move
end #class
