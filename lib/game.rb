require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = @player_one
  end

  def board
    @board
  end

  def play_turn
    move = @player_one.get_move
    @board.place_mark(move, @player_one.mark)
    switch_players!
  end

  def current_player
    @current_player
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
    @current_player = @player_one
    end
    @current_player
  end
end
