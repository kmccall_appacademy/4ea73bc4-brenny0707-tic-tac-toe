class HumanPlayer
  def initialize(name)
    @name = name
    @mark = ""
  end

  def name
    @name
  end

  def board
    @board
  end

  def mark=(mark)
    @mark = mark
  end

  def get_move
    puts "#{@name}, please choose where you'd like to move."
    move = gets.chomp
    position = move.split("").select { |ch| ch.to_i.to_s == ch.to_s}
    position.map { |pos| pos.to_i }
  end

  def display(board)
    puts board.grid
  end
end
